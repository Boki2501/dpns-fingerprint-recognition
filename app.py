import cv2
import matplotlib.pyplot as plt
import numpy as np
import sys
from FingerprintImageEnhancer import FingerprintImageEnhancer
import glob
import os


def enhance_image(img):
    """"Function that enhances image and return the enhanced image"""
    image_enhancer = FingerprintImageEnhancer()
    img = image_enhancer.enhance(img)
    image_enhancer.save_enhanced_image("enhanced/sample_image.jpg")
    result_image = cv2.imread("enhanced/sample_image.jpg")
    return result_image


def get_descriptors(img1, algorithm='SIFT'):
    if algorithm == 'SURF':
        surf = cv2.xfeatures2d.SURF_create(800)
        keypoints, descriptor = surf.detectAndCompute(img1, None)
    elif algorithm == 'ORB':
        orb = cv2.ORB_create()
        keypoints, descriptor = orb.detectAndCompute(img1, None)
    else:
        sift = cv2.xfeatures2d.SIFT_create()
        keypoints, descriptor = sift.detectAndCompute(img1, None)

    return keypoints, descriptor


if __name__ == '__main__':
    while True :
        algorithm = input("Enter algorithm name(SIFT, ORB, SURF):\n")
        if algorithm != 'SIFT' and algorithm != 'ORB' and algorithm != 'SURF':
            print('Algorithm not found.')
            print('Try again...')
        else:
            break
    while True:
        query_image_filepath = input("Input Query Image filepath:\n")
        if query_image_filepath not in os.listdir('dataset'):
            print("Image not found.")
            print("Try again...")
        else:
            break
    while True:
        db_image_filepath = input("Input Database Image filepath:\n")

        if db_image_filepath not in os.listdir('dataset'):
            print('Image not found.')
            print('Try again...')
        else:
            break

    query_image = cv2.imread('dataset/' + query_image_filepath)
    query_image = cv2.cvtColor(query_image, cv2.COLOR_BGR2RGB)
    query_image_gray = cv2.cvtColor(query_image, cv2.COLOR_RGB2GRAY)

    training_image = enhance_image(query_image_gray)
    train_keypoints, train_descriptor = get_descriptors(training_image, algorithm)

    db_image = cv2.imread('dataset/' + db_image_filepath)
    db_image = cv2.cvtColor(db_image, cv2.COLOR_BGR2RGB)
    db_image_gray = cv2.cvtColor(db_image, cv2.COLOR_RGB2GRAY)

    test_image = enhance_image(db_image_gray)
    test_keypoints, test_descriptor = get_descriptors(test_image, algorithm)

    fx, plots = plt.subplots(1, 2, figsize=(20, 10))
    plots[0].set_title("Original Query Image")
    plots[0].imshow(query_image)

    plots[1].set_title("Enhanced Query Image")
    plots[1].imshow(training_image)
    plt.show()

    fx, plots = plt.subplots(1, 2, figsize=(20, 10))
    plots[0].set_title("Original Database Image")
    plots[0].imshow(db_image)

    plots[1].set_title("Enhanced Database Image")
    plots[1].imshow(test_image)
    plt.show()

    # Print the number of keypoints detected in the training image
    print("Number of Keypoints Detected In The Training Image: ", len(train_keypoints))

    # Print the number of keypoints detected in the training image
    print("Number of Keypoints Detected In The Test Image: ", len(test_keypoints))

    keypoints_without_size = np.copy(training_image)
    keypoints_with_size = np.copy(training_image)

    cv2.drawKeypoints(training_image, train_keypoints, keypoints_without_size, color=(0, 255, 0))

    cv2.drawKeypoints(training_image, train_keypoints, keypoints_with_size,
                      flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

    # Display image with and without keypoints size
    fx, plots = plt.subplots(1, 2, figsize=(20, 10))

    plots[0].set_title("Train keypoints With Size")
    plots[0].imshow(keypoints_with_size, cmap='gray')

    plots[1].set_title("Train keypoints Without Size")
    plots[1].imshow(keypoints_without_size, cmap='gray')
    plt.show()

    keypoints_without_size = np.copy(test_image)
    keypoints_with_size = np.copy(test_image)

    cv2.drawKeypoints(test_image, test_keypoints, keypoints_without_size, color=(0, 255, 0))

    cv2.drawKeypoints(test_image, test_keypoints, keypoints_with_size,
                      flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

    # Display image with and without keypoints size
    fx, plots = plt.subplots(1, 2, figsize=(20, 10))

    plots[0].set_title("Test keypoints With Size")
    plots[0].imshow(keypoints_with_size, cmap='gray')

    plots[1].set_title("Test keypoints Without Size")
    plots[1].imshow(keypoints_without_size, cmap='gray')
    plt.show()

    if algorithm == 'SIFT' or algorithm == 'SURF':
        matches = cv2.FlannBasedMatcher(dict(algorithm=1, trees=10),
                                        dict()).knnMatch(train_descriptor, test_descriptor, k=2)
    else:
        FLANN_INDEX_LSH = 6
        index_params = dict(algorithm=FLANN_INDEX_LSH,
                            table_number=6,  # 12
                            key_size=12,  # 20
                            multi_probe_level=1)  # 2
        matches = cv2.FlannBasedMatcher(index_params,
                                        dict()).knnMatch(train_descriptor, test_descriptor, k=2)

    match_points = []

    for i, pair in enumerate(matches):
        try:
            m, n = pair
            if m.distance < 0.1 * n.distance:
                match_points.append(m)
        except ValueError:
            pass

    result = cv2.drawMatches(training_image, train_keypoints, test_image, test_keypoints, match_points, test_image,
                             flags=2)

    # Display the best matching points
    plt.rcParams['figure.figsize'] = [14.0, 7.0]
    plt.title('Best Matching Points')
    plt.imshow(result)
    plt.show()

    # Print total number of matching points between the training and query images
    print("\nNumber of Matching Keypoints Between The Training and Query Images: ", len(match_points))

    if len(match_points) > len(train_keypoints)*0.75:
        print("\nThe images match!")
    else:
        print("\nThe images do not match!")

